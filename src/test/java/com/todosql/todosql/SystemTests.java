package com.todosql.todosql;

import java.time.LocalDate;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.todosql.todosql.entities.Todo;

public class SystemTests {
    @Test
    public void testCreateReadDelete(){
        RestTemplate restTemplate=new RestTemplate();
        String url="http://localhost:8080/todo";
        Todo todo=new Todo(LocalDate.now(),"1","Learn Java",false);

        
        ResponseEntity<Todo>entity=restTemplate.postForEntity(url, todo, Todo.class);
        if(entity.getBody()==null){
            Assertions.assertThat(false);
        }
        ResponseEntity<Todo[]> todos=restTemplate.getForEntity(url, Todo[].class);
        Assertions.assertThat(todos.getBody()).extracting(Todo::getTask).contains("Learn Java");

        restTemplate.delete(url+"/"+entity.getBody().getId());
        todos=restTemplate.getForEntity(url,Todo[].class);

        if(todos.getStatusCode().equals(HttpStatus.NO_CONTENT)){
            Assertions.assertThat(true);
        }
        else Assertions.assertThat(todos.getBody()).extracting(Todo::getId).doesNotContain(entity.getBody().getId());

       

    }
    
}
