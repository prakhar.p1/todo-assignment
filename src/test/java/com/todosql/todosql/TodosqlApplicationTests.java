package com.todosql.todosql;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.todosql.todosql.controller.TodoController;

@SpringBootTest
class TodosqlApplicationTests {
	@Autowired
	TodoController todoController;

	@Test
	void contextLoads() {
		Assertions.assertNotNull(todoController,"TodoController is null");
	}

}
