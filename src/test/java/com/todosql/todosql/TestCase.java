package com.todosql.todosql;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

import com.todosql.todosql.entities.Todo;

public class TestCase {
    public static final String id="1";
    public static final Todo getTodo(){
        return new Todo(LocalDate.now(),id,"Learn Java",false);
    }
    public static final List<Todo> getTodoList(){
        return Arrays.asList(new Todo(LocalDate.now(),id,"Learn Java",false),new Todo(LocalDate.now(),"1","Learn C++",false));
    }
}
