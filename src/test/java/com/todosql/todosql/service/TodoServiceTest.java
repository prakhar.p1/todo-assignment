package com.todosql.todosql.service;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.ResponseEntity;

import com.todosql.todosql.TestCase;
import com.todosql.todosql.dao.TodoRepository;
import com.todosql.todosql.dto.response.TodoResponseInDTO;
import com.todosql.todosql.entities.Todo;
import com.todosql.todosql.mappers.TodoMapper;
import com.todosql.todosql.mappers.TodoResponseInDtoMapper;
import com.todosql.todosql.service.serviceImpl.TodoServiceImpl;

@ExtendWith(MockitoExtension.class)
public class TodoServiceTest {
    @Mock
    private TodoRepository todoRepository;

    private TodoService todoService;


    @BeforeEach
    void setUp(){
        this.todoService=new TodoServiceImpl(todoRepository);
    }

    @Test
    void getAllTodos(){
        todoService.getAllTodos();
        verify(todoRepository).findAll();
    }
    // @Disabled
    // @Test
    // void testFindTodosByDateAdded() {
    //     // Arrange
    //     LocalDate dateAdded = LocalDate.now();
    //     List<Todo> expectedTodos=TestCase.getTodoList();

    //     when(todoRepository.findByDateAdded(dateAdded)).thenReturn(expectedTodos);

    //     // Act

    //     ResponseEntity<List<TodoResponseInDTO>> response=todoService.filterTodosByDate("2024-01-12");

    //     List<Todo> actual=TodoResponseInDtoMapper.todoResponseInDtoListMapper(response.getBody());
    //     assertIterableEquals(expectedTodos,actual);

        
    // }

    @Test
    void testMarkTodoCompleted(){
        String todoId = TestCase.id;
        Todo existingTodo = TestCase.getTodo();

        when(todoRepository.findById(todoId)).thenReturn(Optional.of(existingTodo));
        when(todoRepository.save(existingTodo)).thenReturn(existingTodo);

        // Act
        Todo result = TodoResponseInDtoMapper.todoResponseInDtoMapper(todoService.markTodoCompleted(todoId).getBody());
        assertNotNull(result);
        assertTrue(result.isCompleted());
        verify(todoRepository).findById(todoId);
        verify(todoRepository).save(existingTodo);
    }

}
