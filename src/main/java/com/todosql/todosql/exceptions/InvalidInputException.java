package com.todosql.todosql.exceptions;

import lombok.Getter;

@Getter
public class InvalidInputException extends RuntimeException{
    private String message;
    public InvalidInputException(String message){
        super(message);
        this.message=message;

    }
}
