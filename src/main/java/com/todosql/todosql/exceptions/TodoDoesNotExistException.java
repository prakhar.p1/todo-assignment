package com.todosql.todosql.exceptions;

import lombok.Getter;

@Getter
public class TodoDoesNotExistException extends RuntimeException{
    private String message;
    public TodoDoesNotExistException(String message){
        super(message);
        this.message=message;

    }
}
