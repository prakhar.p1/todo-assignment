package com.todosql.todosql.mappers;



import java.util.List;
import java.util.ArrayList;

import com.todosql.todosql.dto.response.TodoResponseInDTO;
import com.todosql.todosql.entities.Todo;

public class TodoMapper {
    public static final TodoResponseInDTO todoMapper(Todo todo){
        return TodoResponseInDTO
                                .builder()
                                .dateAdded(todo.getDateAdded())
                                .task(todo.getTask())
                                .id(todo.getId())
                                .isCompleted(todo.isCompleted())
                                .build();
    }
    public static final List<TodoResponseInDTO>todoListMapper(List<Todo>todoList){

        List<TodoResponseInDTO> responseList=new ArrayList<>();
        todoList.forEach(todo->responseList.add(todoMapper(todo)));
        return responseList;
    }

    
}
