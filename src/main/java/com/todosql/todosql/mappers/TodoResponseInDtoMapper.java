package com.todosql.todosql.mappers;

import java.util.ArrayList;
import java.util.List;

import com.todosql.todosql.dto.response.TodoResponseInDTO;
import com.todosql.todosql.entities.Todo;

public class TodoResponseInDtoMapper {
    public static final Todo todoResponseInDtoMapper(TodoResponseInDTO todoResponseInDTO){
        return new Todo(todoResponseInDTO.getDateAdded(),todoResponseInDTO.getId(),todoResponseInDTO.getTask(),todoResponseInDTO.isCompleted());
    }
    public static final List<Todo>todoResponseInDtoListMapper(List<TodoResponseInDTO>todoResponseInDTOs){

        List<Todo> responseList=new ArrayList<>();
        todoResponseInDTOs.forEach(todoResponseInDTO->responseList.add(todoResponseInDtoMapper(todoResponseInDTO)));
        return responseList;
    }
}
