package com.todosql.todosql.dao;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.todosql.todosql.entities.Todo;
@Repository
public interface TodoRepository extends JpaRepository<Todo,String> {
    List<Todo>findByDateAdded(LocalDate date);
    Optional<Todo> findById(String id) ;
    List<Todo> findAll();


}
