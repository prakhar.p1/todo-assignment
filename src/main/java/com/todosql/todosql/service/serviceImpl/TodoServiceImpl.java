package com.todosql.todosql.service.serviceImpl;

import java.time.DateTimeException;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.todosql.todosql.dao.TodoRepository;
import com.todosql.todosql.dto.response.TodoResponseInDTO;
import com.todosql.todosql.entities.Todo;
import com.todosql.todosql.exceptions.InvalidInputException;
import com.todosql.todosql.exceptions.TodoDoesNotExistException;
import com.todosql.todosql.mappers.TodoMapper;
import com.todosql.todosql.service.TodoService;

@Service
public class TodoServiceImpl implements TodoService {
    @Autowired
    TodoRepository repository;


    public TodoServiceImpl(TodoRepository todoRepository){
        this.repository=todoRepository;
    }

    public ResponseEntity<List<TodoResponseInDTO>> getAllTodos() {
        List<Todo> currentTodos = repository.findAll();
        if (currentTodos.size() == 0) {
            return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
        }
        return ResponseEntity.status(HttpStatus.OK).body(TodoMapper.todoListMapper(currentTodos));
    }

    public ResponseEntity<TodoResponseInDTO> deleteById(String id) {
        if (id == null) {
            throw new InvalidInputException("Please specify an id");
        }
        Optional<Todo> todoOptional = repository.findById(id);
        if (!todoOptional.isPresent()) {
            throw new TodoDoesNotExistException("Todo with given ID doesn't exist");
        }
        Todo currentTodo = todoOptional.get();
        repository.deleteById(id);
        return ResponseEntity.status(HttpStatus.ACCEPTED).body(TodoMapper.todoMapper(currentTodo));
    }

    public ResponseEntity<TodoResponseInDTO> markTodoCompleted(String id) {
        Optional<Todo> todoOptional = repository.findById(id);
        if (!todoOptional.isPresent()) {
            throw new TodoDoesNotExistException("todo with given id doesn't exists");
        }
        Todo currentTodo = todoOptional.get();
        if (currentTodo.isCompleted()) {
            return ResponseEntity.status(HttpStatus.OK).build();
        }
        currentTodo.setCompleted(true);
        repository.save(currentTodo);
        return ResponseEntity.status(HttpStatus.CREATED).body(TodoMapper.todoMapper(currentTodo));

    }

    public ResponseEntity<TodoResponseInDTO> addTodo(Todo todo) {
        if (todo == null) {
            throw new InvalidInputException("Todo cannot be null");
        }
        if (todo.getTask() == null || todo.getTask().trim().length() == 0) {
            throw new InvalidInputException("please provide valid arguments");
        }
        repository.save(todo);
        return ResponseEntity.status(HttpStatus.CREATED).body(TodoMapper.todoMapper(todo));

    }

    public ResponseEntity<List<TodoResponseInDTO>> filterTodosByDate(String date) {
        try {
            LocalDate currentDate = LocalDate.parse(date);
            List<Todo> currentTodos = repository.findByDateAdded(currentDate);
            if (currentTodos.isEmpty()) {
                return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
            }
            return ResponseEntity.status(HttpStatus.OK).body(TodoMapper.todoListMapper(currentTodos));
        } catch (DateTimeException e) {
            throw new InvalidInputException("Invalid date entered - Please enter it in the format - YYYY-MM-DD");
        }

    }

}
