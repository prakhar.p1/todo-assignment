package com.todosql.todosql.service;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.todosql.todosql.dto.response.TodoResponseInDTO;
import com.todosql.todosql.entities.Todo;

public interface TodoService {
    public ResponseEntity<List<TodoResponseInDTO>> getAllTodos();

    public ResponseEntity<TodoResponseInDTO> deleteById(String id);

    public ResponseEntity<TodoResponseInDTO> markTodoCompleted(String id);

    public ResponseEntity<TodoResponseInDTO> addTodo(Todo todo);

    public ResponseEntity<List<TodoResponseInDTO>> filterTodosByDate(String date);

}
