package com.todosql.todosql.globalExceptionHandler;

import java.util.HashMap;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.todosql.todosql.exceptions.InvalidInputException;
import com.todosql.todosql.exceptions.TodoDoesNotExistException;

@RestControllerAdvice
public class TodoExceptionHandler {
    @ExceptionHandler(InvalidInputException.class)
    public final ResponseEntity<Object> handleInvalidInputException(InvalidInputException ex) {



        Map<String,Object>body=new HashMap<>();
        body.put("mesage",ex.getMessage());
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(body);
        
    }

    

    @ExceptionHandler(TodoDoesNotExistException.class)
    private ResponseEntity<Object>handleTodoDoesNotExistException(TodoDoesNotExistException ex){
        Map<String, Object> body = new HashMap<>();
        body.put("message", ex.getMessage());

        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(body);
    }

   



    
}
