package com.todosql.todosql.controller;

import java.util.List;

import org.springframework.web.bind.annotation.RestController;
import com.todosql.todosql.dto.response.TodoResponseInDTO;
import com.todosql.todosql.entities.Todo;
import com.todosql.todosql.service.TodoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

@RestController
public class TodoController {

    @Autowired
    TodoService todoService;

    @GetMapping("/todo")
    public ResponseEntity<List<TodoResponseInDTO>> getTodos() {

        return todoService.getAllTodos();

    }

    @GetMapping("/todo/date")
    public ResponseEntity<List<TodoResponseInDTO>> getTodosByDate(@RequestParam(value = "date") String date) {
        return todoService.filterTodosByDate(date);

    }

    @PostMapping("/todo")
    public ResponseEntity<TodoResponseInDTO> addTodo(@RequestBody(required = false) Todo todo) {
        return todoService.addTodo(todo);

    }

    @PutMapping("/todo/{id}")
    public ResponseEntity<TodoResponseInDTO> markCompleted(@PathVariable String id) {

        return todoService.markTodoCompleted(id);

    }

    @DeleteMapping("/todo/{id}")
    public ResponseEntity<TodoResponseInDTO> deleteTodo(@PathVariable String id) {
        return todoService.deleteById(id);

    }

}
