package com.todosql.todosql.dto.response;
import java.time.LocalDate;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class TodoResponseInDTO {
    
    @JsonProperty(value = "date_added")
    private LocalDate dateAdded;
   
    private String id;

    private String task;

    @JsonProperty(value="is_completed")
    private boolean isCompleted;


}
